#!/usr/bin/env chez --script

(import (chezscheme)
	(parser)
	(printer)
	(groebner)
	(ordering)
	(variables))

(define (benchmark)
  (define f (parse "-3*x^3*y^2*z+2*x^3*z+x^2*y*z^2"))
  (define g (parse "-x^2*y^2-x^2*y^2-x^3*y^2"))
  (define h (parse "-2*x^3*z+x^2*z^2-3*x^2*y^2*z"))
  (display (format "~{~a~%~}" (map print (groebner+opt f g h)))))

(set-ordering! grevlex)    ;; 単項式順序 (lex, grlex, grevlex)
(set-variables! "xyz") ;; 使用変数
(time (benchmark))     ;; 計測

